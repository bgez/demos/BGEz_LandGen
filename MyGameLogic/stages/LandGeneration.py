from bgez.framework.managers import EventManager as Event
from bgez.framework.managers import EntityManager as EM
from bgez.framework.managers import EventManager
from bgez.framework.managers import StageManager
from bgez.framework.libraries import AssetLibrary
from bgez.framework.objects import Stage
from bgez import Utils

from MyGameLogic.models.Grid import Grid

from mathutils import Vector

@StageManager.MainStage
class LandGeneration(Stage):

    Dependencies = {
        'Grid': AssetLibrary('./MyGameLogic/assets/Grid.blend')
    }

    def __init__(self, **kargs):
        super().__init__(**kargs)
        self.disableSubProcessing()

    def ready(self):
        #EventManager.spread(self.grid.update(self.camera))
        self.enableSubProcessing()

    def start(self):
        self.camera = Utils.getObject('Camera')
        self.camera.Controls.bind(self.camera)

        self.grid = Grid(self.camera, tileSize=128, seed=22)
        self.register(self.grid)

        # self.camera.worldPosition.x = 1 << 18
        print(self.camera.worldPosition)
