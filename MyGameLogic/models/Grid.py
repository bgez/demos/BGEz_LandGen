from bgez.framework.objects import Process
from bgez import Utils

from .Tile import Tile

import weakref

from mathutils import noise
from mathutils import Vector

from collections import namedtuple
Position = namedtuple('Position', ('x', 'y'))

class Grid(Process):

    # Distance culling
    PHYSICS_DISTANCE = 1
    VIEW_DISTANCE = 7
    HYSTERESIS = 1.3

    def __init__(self, cursor, tileSize=16, seed=16):
        super().__init__()
        self.registry = weakref.WeakValueDictionary()
        self.tileSize = tileSize
        self.cursor = cursor
        self.seed = seed

    def getTilePosition(self, worldPosition):
        return Vector((i + self.tileSize / 2) // self.tileSize \
            for i in worldPosition)

    def getTilePositionsAround(self, tilePosition, RANGE=VIEW_DISTANCE):
        tiles = set()
        for i in range(-RANGE, RANGE + 1):
            for j in range(-RANGE, RANGE + 1):
                offset = Vector((i, j, 0))
                if offset.magnitude <= RANGE:
                    tiles.add(Position(
                        x=int(tilePosition.x + i),
                        y=int(tilePosition.y + j),
                    ))
        return tiles

    def getNeighbors(self, tilePosition, RANGE=1):
        neighbors = []
        for i in range(-RANGE, RANGE + 1):
            for j in range(-RANGE, RANGE + 1):
                pos = Position(
                    x = tilePosition.x + i,
                    y = tilePosition.y + j)
                if pos in self.registry:
                    neighbors.append(self.registry[pos])
        return neighbors

    def post_draw(self):
        cursorPosition = self.getTilePosition(self.cursor.worldPosition)
        tiles = self.getTilePositionsAround(cursorPosition)

        for tilePosition in set(self.registry.keys()) - tiles:
            try: # Meh, dead references...
                tile = self.registry[tilePosition]
                delta = tile.worldPosition - self.cursor.worldPosition
                delta.z = 0
                if delta.magnitude >= self.VIEW_DISTANCE * self.HYSTERESIS:
                    del self.registry[tilePosition]
                    tile.destroy()
            except: pass

        for tile in self.getNeighbors(cursorPosition,
        RANGE = self.PHYSICS_DISTANCE):
            if not tile.updatedPhysics:
                tile.updatePhysics()
                break

        for tilePosition in tiles:
            if tilePosition not in self.registry:
                # print('SPAWNED:', tilePosition)
                tile = Tile.Spawn()
                worldPosition = Vector((tilePosition.x, tilePosition.y, 0))
                tile.worldPosition = worldPosition * self.tileSize
                tile.deform(self.getNeighbors(tilePosition), seed=self.seed)
                self.registry[tilePosition] = tile
                break
